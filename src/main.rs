use chrono::NaiveDate;
use std::cmp::Ordering;
use termion::style;

use clap::{ArgMatches, Command};
use io::{config_from_toml, config_to_toml, tasks_data};
/*
use clap::Parser;
use clap_verbosity_flag::{Verbosity, InfoLevel};
*/

#[macro_use]
extern crate clap;

mod io;

fn compare_dates(a: &tasks_data::Task, b: &tasks_data::Task) -> Ordering {
    if a.get_chrono_date() == b.get_chrono_date() {
        return Ordering::Equal;
    } else if a.get_chrono_date() > b.get_chrono_date() {
        return Ordering::Greater;
    } else {
        return Ordering::Less;
    }
}

fn sort_tasks(elements: &mut Vec<tasks_data::Task>, filter: &str) -> () {
    println!(
        "{}{}{}",
        style::Bold,
        "Sorting tasks by:".to_string(),
        filter
    );
    match filter {
        "default" => {}
        "default_inverse" => {
            elements.sort_by(|a, b| b.get_id().cmp(&a.get_id()));
        }
        "date" => elements.sort_by(|a, b| compare_dates(a, b)),
        "date_inverse" => elements.sort_by(|a, b| compare_dates(b, a)),
        _ => {}
    }
}

fn filter_by_category(elements: &mut Vec<tasks_data::Task>, filter: &str) -> () {
    println!(
        "{}{}{}",
        style::Bold,
        "Filtering tasks by:".to_string(),
        filter
    );
    elements.retain(|task : &tasks_data::Task| -> bool {
        for cat in task.get_cats() {
            if cat == filter {
                return true;
            }
        }
        return false;
    });
}

fn list_tasks(elements: Vec<io::tasks_data::Task>) -> () {
    println!(
        "{}{1: <6} {2: <32} {3: <12} Categories {4}",
        style::Bold,
        "Id",
        "Goal",
        "Exp_date",
        style::Reset
    );
    for task in elements {
        print!(
            "{0: <6} {1: <32} {2: <12}",
            task.get_id(),
            task.get_goal(),
            task.get_date_string()
        );
        for tc in task.get_cats() {
            print!(" {}", tc);
        }
        print!("\n");
    }
}

fn main() {
    // Cli with Clap library
    let matches: ArgMatches = Command::new("nxdo")
        .author("poposca, poposca@gmail.com")
        .version("0.6.1")
        .about("Simple TODO utility for your terminal. Written in rust.")
        .arg(arg!(-i --init "Sets a blank data file"))
        // Add task subcommand
        .subcommand(
            Command::new("add")
                .about("Add new task")
                .version("0.1")
                .author("poposca")
                .arg(arg!(
                    <GOAL> "Name or goal of the task."
                ))
                .arg(arg!(
                    <DATE> "Expiring date of the task (mm-dd-yyyy)."
                ))
                .arg(arg!(
                    <CATS> "Groups or categories the task is part of."
                )),
        )
        // Complete task subcommand
        .subcommand(
            Command::new("complete")
                .about("Eliminate task from to-do list")
                .version("0.1")
                .author("poposca")
                .arg(arg!(
                    <ID> "Identification number of the task that has been completed"
                )),
        )
        // List tasks subcommand
        .subcommand(
            Command::new("list")
                .about("Lists all the pending tasks")
                .version("0.1")
                .author("poposca")
                .arg(
                    arg!(
                        -o --"order-by" <ORDER_BY> "Way in which the tasks should be listed"
                    )
                    .action(clap::ArgAction::Set),
                )
                .arg(
                    arg!(
                        -c --category <CATEGORY> "Category to filter the tasks with"
                    )
                    .action(clap::ArgAction::Set),
                ),
        )
        // Categories subcommand
        .subcommand(
            Command::new("cats")
                .about("Administrate categories")
                .version("0.1")
                .author("poposca")
                .subcommand(
                    Command::new("add")
                        .about("Add a new category")
                        .version("0.1")
                        .author("poposca")
                        .arg(arg!(
                            <NAME> "Name of the category"
                        )),
                )
                .subcommand(
                    Command::new("remove")
                        .about("Remove a category")
                        .version("0.1")
                        .author("poposca")
                        .arg(arg!(
                            <NAME> "Name of the category"
                        )),
                )
                .subcommand(
                    Command::new("list")
                        .about("List all the categories")
                        .version("0.1")
                        .author("poposca"),
                ),
        )
        // Locale subcommand
        .subcommand(
            Command::new("locale")
                .about("Administrate locales")
                .version("0.1")
                .author("poposca")
                .subcommand(
                    Command::new("set")
                        .about("Change the locale value. Possible values: spanish | english")
                        .version("0.1")
                        .author("poposca")
                        .arg(arg!(
                            <LOC> "Name of the locale"
                        )),
                ),
        )
        .get_matches();

    // Load config file
    let mut config: tasks_data::Config = config_from_toml();

    // --- ADMINISTRATE MATCHES ---
    // Subcommand *list*
    if let Some(matches) = matches.subcommand_matches("list") {
        let mut temp_vec: Vec<tasks_data::Task> = config.tasks;
        if matches.contains_id("category") {
            let _category: &String = matches.get_one::<String>("category").unwrap();
            filter_by_category(&mut temp_vec, _category); 
        }
        if matches.contains_id("order-by") {
            // Impresión de tareas con orden personalizado
            match matches.try_get_one::<String>("order-by") {
                Err(err) => {
                    println!("{}{}", style::Bold, "Error parsing flag value".to_string());
                    println!("{}{}", err, style::Reset);
                    sort_tasks(&mut temp_vec, "default");
                }
                Ok(filter_option) => match filter_option.unwrap().as_str() {
                    "date" => {
                        sort_tasks(&mut temp_vec, "date");
                    }
                    "idate" => {
                        sort_tasks(&mut temp_vec, "date_inverse");
                    }
                    "inverse" => {
                        sort_tasks(&mut temp_vec, "default_inverse");
                    }
                    _ => {
                        sort_tasks(&mut temp_vec, "default");
                    }
                },
            };
        } else {
            // Impresión normal de todas las tareas
            // println!("{}{}", style::Bold, "No flag introduced to list subcommand".to_string());
            sort_tasks(&mut temp_vec, "default");
        }
        list_tasks(temp_vec);
    }
    // Subcommand *add*
    else if let Some(matches) = matches.subcommand_matches("add") {
        //introducing categories and verifying existence
        let cats: Vec<String> = matches
            .get_many::<String>("CATS")
            .unwrap()
            .map(|x| String::from(x))
            .collect();
        let mut filtered_categories = Vec::new();
        let mut existence = true;
        for j in cats {
            if !config.cats.contains(&String::from(j.clone())) {
                println!("'{}' is not a valid category.", j);
                existence = false;
            }
            filtered_categories.push(j);
        }
        //If all categories exist, introduce all arguments
        if existence {
            let goal_argument: &String = matches.get_one::<String>("GOAL").unwrap();
            let date_argument: &String = matches.get_one::<String>("DATE").unwrap();
            let task_id: u16 = config
                .tasks
                .last()
                .unwrap_or(&tasks_data::Task::new(
                    0,
                    "Do",
                    "01-01-2020",
                    vec![String::from("a"), String::from("b")],
                ))
                .get_id()
                + 1;

            let dt: NaiveDate =
                tasks_data::parse_naive_date(&date_argument, &config.locale).unwrap();

            config.tasks.push(tasks_data::Task::new(
                task_id as u16,
                &goal_argument,
                dt.to_string().as_str(),
                filtered_categories,
            ));
            config_to_toml(config);
            println!("Task added succesfully");
        } else {
            println!("Task not registered.");
        }
    }
    // Subcommand *complete*
    else if let Some(matches) = matches.subcommand_matches("complete") {
        let index: u16 = *matches.get_one::<u16>("ID").unwrap();
        tasks_data::call_on_task(
            &mut config.tasks,
            |task: &mut tasks_data::Task| task.get_id() == index,
            |t| t.complete_task(),
        );
        config_to_toml(config);
        println!("Task removed succesfully");
    }
    // Subcommand *cats*
    else if let Some(matches) = matches.subcommand_matches("cats") {
        let cats_array: Vec<String> = matches
            .get_many::<String>("CATEGORIES")
            .unwrap()
            .map(|x| String::from(x))
            .collect();

        // Subcommands of cats subcommand
        // ---
        if let Some(_matches) = matches.subcommand_matches("add") {
            for cat in cats_array {
                let temp_cat: &String = &String::from(cat.clone()).to_lowercase();
                if !config.cats.contains(&String::from(temp_cat)) {
                    config.cats.push(String::from(temp_cat));
                } else {
                    println!(
                        "Category '{}' is already registered. Operation not allowed!",
                        cat
                    );
                }
            }
            config_to_toml(config);
        } else if let Some(_matches) = matches.subcommand_matches("delete") {
            for cat in cats_array {
                if config.cats.contains(&String::from(cat.clone())) {
                    config
                        .cats
                        .retain(|t| t.to_lowercase() == String::from(cat.clone()).to_lowercase());
                } else {
                    println!("Category '{}' is not registered. Imposible to remove!", cat);
                }
            }
            config_to_toml(config);
        } else if let Some(_matches) = matches.subcommand_matches("list") {
            for c in config.cats {
                println!("{}", c);
            }
        } else {
            for c in config.cats {
                println!("{}", c);
            }
        }
    }
    // Subcommand *locale*
    else if let Some(matches) = matches.subcommand_matches("locale") {
        if let Some(matches) = matches.subcommand_matches("set") {
            let loc: &String = matches.get_one::<String>("LOC").unwrap();
            config.set_locale(loc);
            config_to_toml(config);
            println!("Locale correctly changed to {}!", loc);
        } else {
            let l = config.locale;
            println!("Locale = {}", l);
        }
    }
    //No subcommands
    else {
        if matches.get_flag("init") {
            let contenido: String = io::default_config();
            io::save_config(&contenido);
            println!("Written void data file in:'{}'", io::config_file());
        } else {
            list_tasks(config.tasks);
        }
    }
}
