use serde::Deserialize;
use serde::Serialize;
use std::fmt::Write;
use std::str::FromStr;

use chrono::prelude::*;
use chrono::ParseError;

use phf::phf_map;

static LOCALE_MAP: phf::Map<&str, &str> = phf_map! {
    "english" => "%m-%d-%Y",
    "spanish" => "%d-%m-%Y",
    "default" => "%Y-%m-%d",
};

#[derive(Serialize, Deserialize, Clone)]
pub struct Config {
    pub locale: String,
    pub cats: Vec<String>,
    pub tasks: Vec<Task>,
}

#[derive(Serialize, Deserialize, Clone)]
pub struct Task {
    id: u16,
    goal: String,
    exp_date: toml::value::Datetime,
    categories: Vec<String>,
    completed: bool,
}

impl Task {
    pub fn new(id: u16, goal: &str, date: &str, categories: Vec<String>) -> Self {
        let mut temp: Vec<String> = Vec::new();
        for item in categories {
            temp.push(String::from(item));
        }
        Self {
            id: id,
            goal: String::from(goal),
            exp_date: toml::value::Datetime::from_str(date)
                .unwrap_or(toml::value::Datetime::from_str("0000-01-01").unwrap()),
            categories: temp,
            completed: false,
        }
    }

    pub fn get_id(&self) -> u16 {
        return self.id;
    }

    pub fn get_goal(&self) -> &String {
        return &self.goal;
    }

    pub fn get_date_string(&self) -> String {
        let mut asd: String = String::new();
        write!(asd, "{}", &self.exp_date).expect("Unable to write to string buffer.");
        return asd;
    }

    pub fn get_chrono_date(&self) -> chrono::NaiveDate {
        let asd: String = self.get_date_string();
        let date: NaiveDate = parse_naive_date(&asd, &String::from("default")).unwrap();
        return date;
    }

    pub fn get_cats(&self) -> &Vec<String> {
        return &self.categories;
    }

    pub fn complete_task(&mut self) -> () {
        self.completed = true;
    }
}

impl Config {
    pub fn set_locale(&mut self, loc: &str) -> () {
        match loc {
            "english" => self.locale = String::from(loc),
            "spanish" => self.locale = String::from(loc),
            _ => println!("Locale {} is invalid", loc),
        }
    }
}

pub fn call_on_task(
    obj_list: &mut Vec<Task>,
    mut condition: impl FnMut(&mut Task) -> bool,
    mut func: impl FnMut(&mut Task) -> (),
) {
    for x in obj_list {
        if condition(x) {
            func(x);
        }
    }
}

pub fn format_string(locale_key: &str) -> &str {
    return LOCALE_MAP.get(locale_key).unwrap_or(&"%Y-%m-%d");
}

pub fn parse_naive_date(date: &str, date_locale: &String) -> Result<chrono::NaiveDate, ParseError> {
    // FAILS TO UNWRAP
    // Track down where it loads from toml file
    // Does it store it always as Y-m-d?
    // only use this function when reading user input. Then use default format.
    let format_string: &str = format_string(date_locale);
    let temp_date: Result<NaiveDate, chrono::ParseError> =
        NaiveDate::parse_from_str(date, format_string);
    //println!("{} :: {}", date, format_string);
    let date = match temp_date {
        Ok(dt) => dt,
        Err(error_msg) => {
            println!("Error parsing date: {}", error_msg);
            return Err(error_msg);
        }
    };
    return Ok(date);
}
