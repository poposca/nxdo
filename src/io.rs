use std::fs::File;
use std::io::prelude::*;
use dirs::home_dir;
use std::path::PathBuf;

#[path = "tasks_data.rs"]
pub mod tasks_data;

pub fn config_file() -> String {
    //Obtener nombre del archivo de datos
    let mut user_dir = home_dir().unwrap_or(PathBuf::from(""))
                                                  .into_os_string()
                                                  .into_string()
                                                  .unwrap();
    user_dir.push_str("/.config/nxdo.toml");
    return user_dir;
}

pub fn default_config() -> String {
    let conf = tasks_data::Config{
        locale: String::from("spanish"),
        cats: vec!(String::from("important"), String::from("optional")),
        tasks: Vec::new()
    };
    let text = toml::to_string(&conf).unwrap();
    return text;
}

fn load_config() -> String {
    //Abrir archivo de datos y guardarlo como String en *contenido*
    let mut contenido = String::new();
    match File::open(config_file()) {
        Err(e) => { 
            println!("Tasks file not found. Cause:{:?} \n Creating one...", e);
            contenido = default_config();
            save_config(&contenido);
        },
        Ok (mut g) => {
            g.read_to_string(&mut contenido).expect("Failed to read file into a String.");
        },
    };
    return contenido;
}

pub fn save_config(info : &String) -> (){
    std::fs::write(config_file(), info).expect("Could not write data and configuration file.");
}

pub fn config_from_toml() -> tasks_data::Config{
    //Carga configuración en *config_text*
    let configtext = load_config();

    //Parce with toml&serde: 
    let config : tasks_data::Config = toml::from_str(&configtext).unwrap();
    return config;
}

pub fn config_to_toml(conf : tasks_data::Config) -> () {
    let text = toml::to_string(&conf).unwrap();
    save_config(&text);
}
